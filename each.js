function each(elements,cb){
    if (Array.isArray(elements)){
        let result_array=[];
        for (let index=0;index<elements.length;index++){
            result_array.push(cb(elements[index],index))
        }
        return result_array
    }else{
        return elements
    }
}
module.exports=each