const result=[];
function flatten(elements,depth){
    if (depth===undefined){
        depth=1;
    }
    for (let index=0;index<elements.length;index++){
        if (typeof (elements[index])==="number" || depth===0){
            result.push(elements[index]);
        }
        else if(elements[index]===undefined){
            continue;
        }else if ((!Array.isArray(elements[index]))){
            result.push(elements[index]);
        }
        else if (depth>0){
            flatten(elements[index],depth-1);
        }
    }
    return result;
}
module.exports=flatten;