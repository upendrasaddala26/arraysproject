module.exports=function reduce(elements,callback,startingValue){
    if(!Array.isArray(elements) || !callback){
        return [];
    }
    let startingindex=0;
    if (startingValue===undefined){
        startingValue = elements[0];
        startingindex++;
    }
    
    for (let index=startingindex;index<elements.length;index++){
        
        startingValue = callback(startingValue,elements[index],index,elements);
    } 
    return startingValue;
    
};
